from simple_salesforce import Salesforce
import json
from datetime import date, datetime
import os
import time
import sys

# Set data type mapping
mapping =\
{'id':'varchar',\
'boolean':'bool',\
'reference':'varchar',\
'string':'varchar',\
'picklist':'varchar',\
'textarea':'varchar',\
'double':'decimal',\
'phone':'varchar',\
'url':'varchar',\
'currency':'double',\
'int':'int',\
'datetime':'timestamp',\
'date':'timestamp',\
'email':'varchar',\
'multipicklist':'varchar',\
'percent':'decimal',\
'decimal':'decimal',\
'long':'bigint',\
'address':'varchar',\
'masterrecord':'varchar',\
'location':'varchar',\
'encryptedstring':'varchar'}

name = 'anton.karpenko@ucop.edu'
pwd = 'API PWD'
tkn = 'TOKEN'

campuses = ["UCD", "UCB", "UCI", "UCM", "UCSB", "UCLA", "UCSC", "UCSD", "UCR"]
# Just single campus for fast debug
#campuses = ["UCR"]
#
coursesCurrentTerm = []
coursesPreviousTerm = []
coursesCurrentTermEnrollments = []
coursesPreviousTermEnrollments = []
previousSeason = ''
filename = os.path.dirname(os.path.realpath(__file__))+'/report.log'

def dump(obj):
    for attr in dir(obj):
        print("obj.%s = %r" % (attr, getattr(obj, attr)))

# Getting current season
Y = 2000 # dummy leap year to allow input X-02-29 (leap day)
seasons = [('Winter', (date(Y,  1,  1),  date(Y,  3, 20))),
           ('Spring', (date(Y,  3, 21),  date(Y,  6, 20))),
           ('Summer', (date(Y,  6, 21),  date(Y,  9, 22))),
           ('Fall', (date(Y,  9, 23),  date(Y, 12, 20))),
           ('Winter', (date(Y, 12, 21),  date(Y, 12, 31)))]

def get_season(now):
    if isinstance(now, datetime):
        now = now.date()
    now = now.replace(year=Y)
    return next(season for season, (start, end) in seasons
                if start <= now <= end)

def duplicate(items): 
    unique = [] 
    for item in items: 
        if item not in unique: 
            unique.append(item) 
    return unique 

currentSeason = get_season(date.today())
now = datetime.now()
year = now.year

#if currentSeason == 'Winter':
#    previousSeason = 'Fall'
#elif currentSeason == 'Fall':
#    previousSeason = 'Spring'
#elif currentSeason == 'Spring':
#    previousSeason = 'Winter'

previousSeason = currentSeason

# Checking for log file
if os.path.exists(filename):
    open(filename, 'w').close()
    append_write = 'a' # append if already exists
else:
    append_write = 'w' # make a new file if not
# Changing year if term is winter
letsWrite = open(filename,append_write)
    
#if currentSeason == 'Winter':
#    yearPrevious = int(year) - 1
#else:
#    yearPrevious = int(year)
    
if currentSeason == 'Summer':
    previousSeason = 'Summer'
    yearPrevious = int(year) - 1

yearPrevious = int(year) - 1

for campus in campuses:
    
    # Calling SalesForce connector
    sf = Salesforce(username=name, password=pwd,\
        security_token=tkn, version='45.0', sandbox=False)
    
    # Making a direct API call to list avaialble REST API Versions
    try:
        resultAPI = sf.restful('limits/', params=None)
    except:
        sys.exit('Test API call failed!')
    
    print('Sleeping for 60 seconds...')
    print('')
    time.sleep( 60 )
    if (campus == 'UCB' or campus == 'UCM'):
        term = 'Semester'
        if currentSeason == 'Winter':
            currentSeason = 'Fall'
        if previousSeason == 'Winter':
            previousSeason = 'Fall'            
    else:
        term = 'Quarter'
    # Setting correct term
    campusTerm = currentSeason+' '+term+' '+str(year)
    campusPreviusTerm = previousSeason+' '+term+' '+str(yearPrevious)
    #
    print('Quarrying campus: '+campus+'. Term: '+campusTerm)
    letsWrite.write('Quarrying campus: '+campus+'. Term: '+campusTerm+'\n')
    print('Previous term for '+campus+' will be: '+campusPreviusTerm)
    letsWrite.write('Last term for '+campus+' will be: '+campusPreviusTerm+'\n')
    print('')
    letsWrite.write(' \n')
    # Getting courses for curent term 
    results = sf.query('SELECT Course__r.Name FROM Offering__c WHERE Campus_Term_Events__r.Name = \''+campusPreviusTerm+' '+campus+'\'')
    resultsJSON = json.dumps(results)
    resultsJSONLoaded = json.loads(resultsJSON)
    records = resultsJSONLoaded['records']
    for record in records:
        course = (record['Course__r']['Name'])
        coursesPreviousTerm.append(course)
    # Getting courses for previous terms
    results = sf.query('SELECT Course__r.Name FROM Offering__c WHERE Campus_Term_Events__r.Name = \''+campusTerm+' '+campus+'\'')
    resultsJSON = json.dumps(results)
    resultsJSONLoaded = json.loads(resultsJSON)
    records = resultsJSONLoaded['records']
    for record in records:
        course = (record['Course__r']['Name'])
        coursesCurrentTerm.append(course)  
    # Comparing two arrays, comparing two course lists
    coursesMatchedTerm = []
    # Removing duplicates
    coursesCurrentTerm = duplicate(coursesCurrentTerm)
    coursesPreviousTerm = duplicate(coursesPreviousTerm)
    coursesMatchedTerm = set(coursesCurrentTerm) & set(coursesPreviousTerm)
    #coursesMatchedTerm = list((set(coursesCurrentTerm) | set(coursesPreviousTerm)) - (set(coursesCurrentTerm) & set(coursesPreviousTerm)))
    # Getting course enrollment numbers for current and previous term    
    for courseMatchedTerm in coursesMatchedTerm:
        # Debug 
        #print('Course '+courseMatchedTerm+' and Term '+campusTerm)
        #print('Course '+courseMatchedTerm+' and Term '+campusPreviusTerm)
        #
        query = 'SELECT COUNT() FROM Campus_Enrollment__c WHERE Approval_Status__c = \'Enrolled\' AND Is_Host_Campus_Enrollment__c = TRUE AND homecampus__c = \''+campus+'\' AND Term_Name__c = \''+campusTerm+'\' AND Course__c = \''+courseMatchedTerm+'\''
        result = sf.query(query)
        query = ''
        query = 'SELECT COUNT() FROM Campus_Enrollment__c WHERE Approval_Status__c = \'Enrolled\' AND Is_Host_Campus_Enrollment__c = TRUE AND homecampus__c = \''+campus+'\' AND Term_Name__c = \''+campusPreviusTerm+'\' AND Course__c = \''+courseMatchedTerm+'\''
        resultPrevious = sf.query(query)
        query = ''
        # Debug         
        #print(format(result['totalSize']))
        #print(format(resultPrevious['totalSize']))
        #
        coursesCurrentTermEnrollments.append(format(result['totalSize']))
        coursesPreviousTermEnrollments.append(format(resultPrevious['totalSize']))
        # Clearing vars just in case
        result = ''
        resultPrevious = '' 
    coursesLength = len(coursesMatchedTerm)
    coursesMatchedTermList = list(coursesMatchedTerm)
    counter = 1
    while counter <= coursesLength:
        print('Campus: '+campus)
        letsWrite.write('Campus: '+campus+'\n')
        print('Course: '+coursesMatchedTermList[counter - 1])
        letsWrite.write('Course: '+coursesMatchedTermList[counter - 1]+'\n')
        print('This term: ' + coursesCurrentTermEnrollments[counter - 1])
        letsWrite.write('This term: ' + coursesCurrentTermEnrollments[counter - 1]+'\n')
        print('Previous term: ' + coursesPreviousTermEnrollments[counter - 1])
        letsWrite.write('Previous term: ' + coursesPreviousTermEnrollments[counter - 1]+'\n')
        percentage = int(coursesPreviousTermEnrollments[counter - 1]) / 10
        calc = int(coursesCurrentTermEnrollments[counter - 1]) - int(coursesPreviousTermEnrollments[counter - 1])
        if calc < 0:
            if abs(calc) > percentage:
                print('Check course '+coursesMatchedTermList[counter - 1]+'. It has low numbers!')
                letsWrite.write('Check course '+coursesMatchedTermList[counter - 1]+'. It has low numbers!\n')
            else:
                print('Course '+coursesMatchedTermList[counter - 1]+' is OK.')
                letsWrite.write('Course '+coursesMatchedTermList[counter - 1]+' is OK.\n')
        elif (int(coursesPreviousTermEnrollments[counter - 1]) == 0) and (int(coursesCurrentTermEnrollments[counter - 1]) == 0):
            print('Course '+coursesMatchedTermList[counter - 1]+' has 0 enrollmets!')
            letsWrite.write('Course '+coursesMatchedTermList[counter - 1]+' has 0 enrollmets!\n')
        else:
            print('Course '+coursesMatchedTermList[counter - 1]+' is OK.')
            letsWrite.write('Course '+coursesMatchedTermList[counter - 1]+' is OK.\n')
        print('')
        letsWrite.write(' \n')
        counter = counter + 1
    print('End report for '+campus+'.')
    letsWrite.write('End of report for '+campus+'.\n')
    print('============================================================')
    letsWrite.write('============================================================\n')
    time.sleep( 5 )
print('')
letsWrite.close()
print('Done.')